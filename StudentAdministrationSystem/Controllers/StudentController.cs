﻿using StudentAdministrationSystem.Models;
using StudentAdministrationSystem.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentAdministrationSystem.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View(StudentStorage.GetAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student student)
        {
            StudentStorage.Add(student);
            return View("Index", StudentStorage.GetAll());
        } 

        public ActionResult Details(int id)
        {
            return View(StudentStorage.Get(id));
        }

        public ActionResult Enroll(int id)
        {
            EnrollStudentViewModel enrollStudent = new EnrollStudentViewModel();
            enrollStudent.Student = StudentStorage.Get(id);
            enrollStudent.Subjects = SubjectStorage.GetAllBut(enrollStudent.Student.Subjects);
            return View(enrollStudent);
        }

        [HttpPost]
        public ActionResult Enroll(EnrollStudentViewModel enrollStudent)
        {
            Student student = StudentStorage.Get(enrollStudent.Student.StudentId);
            enrollStudent.SelectedSubject = SubjectStorage.Get(enrollStudent.SelectedValue);
            student.Subjects.Add(enrollStudent.SelectedSubject);
            enrollStudent.Student = student;
            return View("EnrollDetails",enrollStudent);
        }
    }
}